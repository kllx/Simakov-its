const express = require('express')
const bodyParser = require('body-parser')
// const config = require('./config')
// const Recaptcha = require('express-recaptcha').Recaptcha

let app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
