const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const config = require('./config')

const app = express()
// создаем парсер для данных application/x-www-form-urlencoded
const urlencodedParser = bodyParser.urlencoded({ extended: false })

const DataFolder = config.data

function getDirectories (path, callback) { // функция обертка над readdir
  fs.readdir(path, (err, content) => {
    if (err) return callback(err)
    callback(null, content)
  })
}

app.use('/public', express.static('public'))
app.set('view engine', 'ejs')

app.get('/', (_req, res) => {
  getDirectories(DataFolder, (_err, content) => {
    res.render('index', { 'titleName': content })
  })
})
app.get('/articles/:title', (req, res) => {
  getDirectories(DataFolder, (_arr, content) => { // смотрим содержимое data
    if (content.indexOf(req.params.title) !== -1) { // проверяем существует ли фаил со статьей
      fs.readFile(DataFolder + '/' + req.params.title, 'utf8', (_err, text) => { // считываем фаил со статьей
        const obj = JSON.parse(text)
        res.render('article', { // рендерим статью
          Title: req.params.title,
          Text: obj
        })
      })
    } else {
      res.render('404', {
        msg: 'Такой статьи не существует'
      })
    }
  })
})
app.get('/add', (_req, res) => { // добавление статьи
  res.render('add')
})
app.post('/add', urlencodedParser, (req, res) => { // функция создания статьи
  if (!req.body) return res.sendStatus(400) // проверка на наличие информации в запросе
  console.log(req.body)
  getDirectories(DataFolder, (_arr, content) => {
    if (content.indexOf(req.body.title + '.json') === -1) { // проверка на уникальность названия статьи
      let dataFirstChapter = { titleChapter1: req.body.titleChapter1, textChapter1: req.body.textChapter1 } // запись данных первой главы
      let dataSecChapter = { titleChapter2: req.body.titleChapter2, textChapter2: req.body.textChapter2 } // запись данных второй главы
      const articleData = JSON.stringify({ articleTitle: req.body.title, FirstChapter: dataFirstChapter, SecondChapter: dataSecChapter }) // соединение глав в единый объект статьи и перевод в JSON формат
      fs.writeFileSync(DataFolder + '/' + req.body.title + '.json', articleData) // создание нового файла с данными статьей
      res.redirect('/') // редирект на главную
    } else {
      res.render('404', {
        msg: 'Статья с таким именем уже существует'
      })
    }
  })
})

app.get('/del', (_req, res) => {
  getDirectories(DataFolder, (_err, content) => {
    res.render('del', { 'titleName': content })
  })
})

app.post('/del', urlencodedParser, (req, res) => {
  if (!req.body) return res.sendStatus(400)
  getDirectories(DataFolder, (_arr, content) => {
    if (content.indexOf(req.body.del + '.json') !== -1) { // проверка на существование файла переданной статьи
      fs.unlink(`data/${(req.body.del)}.json`, () => {}) // удаление переданной файла статьи
      res.redirect('/') // редирект на главную
    } else {
      res.render('404', {
        msg: 'Введено неверное название'
      })
    }
  })
})
app.use('/', (req, res) => {
  res.status(404)
  res.send('404')
})

app.listen(config.port)
