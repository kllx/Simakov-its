const fs = require('fs');

function articleText2json(text) {
    const re = /Глава\d*:\W*\n/gmu;
    return [text.match(re), text.split(re)];
    // return ChapTitle;
}

fs.readFile('text.txt', 'utf-8', function (err, data) {
    let text = JSON.stringify(articleText2json(data));
    console.log(text);
});
