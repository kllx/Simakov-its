const  http = require('http');
const fs = require('fs');
// HTTP
// let server = http.createServer((req, res) => {
//     console.log("URL страницы: " + req.url);
//     res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
//     let myReadShort = fs.createReadStream(__dirname + '/index.html', 'utf-8');
//
//     myReadShort.pipe(res);
// });
// JSON

let server = http.createServer((req, res) => {
    console.log("URL страницы: " + req.url);
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    let obj = {FirstChapter:{titleChapter1: 'Глава:Полет на марс', textChapter1:'Lorem ipsum dolor sit amet, consectetur adipiscing\r\nelit, sed do eiusmod tempor incididunt ut labore et dolore\r\nagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation u\r\nllamco\r\nlaboris nisi ut aliquip ex ea commodo consequat. Duis aute irure \r\ndolor in reprehenderit\r\nin voluptate velit esse cillum d\r\nolore eu fugiat\r\nnulla pariatur. Excepteur sint occ\r\naecat',
            SecondChapter:{titleChapter2: 'Глава2: Первый контакт', textChapter2:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\nAt risus viverra adipiscing at in tellus integer.'}}};
    res.end(JSON.stringify(obj.FirstChapter));
});

server.listen(3000, '127.0.0.1');
console.log("Отслеживаем порт 3000");
