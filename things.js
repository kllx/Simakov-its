const some_value = 451;
const array_counter = (array) => {
  return "В массие находиться " + array.length + " элементов!";
};

const multiply = (x, y) => {
  return `${x} умножить ${y} равно ${x * y}`;
};

module.exports = {
    some_value: some_value,
    multiply: multiply,
    array_counter: array_counter,
};
