const fs = require('fs');

let myReadShort = fs.createReadStream(__dirname + '/big.txt', 'utf-8');
let myWhiteShort = fs.createWriteStream(__dirname + '/news.txt');

// myReadShort.on('data', (chunk) => {
//     console.log("Новые данные получены\n");
//     myWhiteShort.write(chunk);
// });

myReadShort.pipe(myWhiteShort);
